       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA2.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SURNAME           PIC X(8)     VALUE "COUGTLAN".
       01  SALE-PRICE        PIC 9(4)V99.
       01  NUM-OF-EMPLOYEE   PIC 999V99.   
       01  SALARY            PIC 9999V99.
       01  COUNTY-NAME       PIC X(9).
       PROCEDURE DIVISION .
       Begin.
           DISPLAY "1 " SURNAME 
           MOVE "SMITH" TO SURNAME .
           DISPLAY "2 " SURNAME 
           MOVE "FITWILLAM" TO SURNAME .
           DISPLAY "3 " SURNAME .
           DISPLAY "1 " SALE-PRICE 
           MOVE ZEROS TO SALE-PRICE .
           DISPLAY "2 " SALE-PRICE .
           MOVE 25.5 TO SALE-PRICE .
           DISPLAY "3 " SALE-PRICE .
      *     01 SALE-PRICE PIC 9(4)V99. 0007.55 
           MOVE 7.553 TO SALE-PRICE 
           DISPLAY "4 " SALE-PRICE .
      *     01 SALE-PRICE PIC 9(4)V99. 3425.15      
           MOVE 93245.158 TO SALE-PRICE
           DISPLAY "5 " SALE-PRICE .
      *     01 SALE-PRICE PIC 9(4)V99. 0128.00   
           MOVE 128 TO SALE-PRICE .
           DISPLAY "6 " SALE-PRICE .

      *      01 SALE-PRICE PIC 9(4)V99. 0128.00 
           DISPLAY NUM-OF-EMPLOYEE .
           MOVE 12.4 TO NUM-OF-EMPLOYEE .
           DISPLAY NUM-OF-EMPLOYEE .
           MOVE 6745 TO NUM-OF-EMPLOYEE .
           DISPLAY NUM-OF-EMPLOYEE .
           MOVE NUM-OF-EMPLOYEE TO SALARY .
           DISPLAY SALARY .

           MOVE "GALWAY" TO COUNTY-NAME .
           DISPLAY COUNTY-NAME .
           MOVE ALL "-*" TO COUNTY-NAME .
           DISPLAY COUNTY-NAME .

