       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA4.
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  STUDENT-REC-DATA PIC X(44) VALUE "1205621William   Fitpatrick
      -     "19751021LM051385" .
       01  LONG-STR PIC X(200)  VALUE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      -     "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" .
       01  STUDENT-REC.
           05 STUDENT-ID     PIC 9(7).
           05 STUDENT-NAME.
              10 FORNAME     PIC X(9).
              10 SURNAME     PIC X(12).
           05 DATE-OF-BIRTH.
              10 YOB         PIC 9(4).
              10 MOB         PIC 99.
              10 DOB         PIC 99.
           05 CORUSE-ID      PIC X(5).
           05 GPA            PIC 9V99.
       PROCEDURE DIVISION .
           DISPLAY STUDENT-REC-DATA 
           MOVE STUDENT-REC-DATA TO STUDENT-REC 
           DISPLAY STUDENT-REC 
           DISPLAY STUDENT-ID 
           DISPLAY STUDENT-NAME
           DISPLAY FORNAME 
           DISPLAY SURNAME 
           DISPLAY DATE-OF-BIRTH 
           DISPLAY CORUSE-ID 
           DISPLAY GPA 
           .